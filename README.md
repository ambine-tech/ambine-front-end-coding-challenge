# Test task

Your task is to create a simple weather app following the given design.
The app consists of a search field and weather widgets.
[You can see it in action here](https://xd.adobe.com/view/6fc37379-94f1-4d01-af28-8bdc2b8888e3-3a90/)


## Requirements

A user should be able to type a city name or a part of it in the search field and see suggested 
results. Each row of the suggestion contains a city name and an “Add” button.
When clicking on that button, the user expects to see a new widget appear on the page among the other widgets.
The widget should be added to the end of the list. 
The user also expects to be able to remove any widget from the list.

You may also omit certain parts of the requirements.
For example, the search field with suggestions might be replaced with an ordinary input without
suggestions (this way, your code just consumes the value of the input and widget is being added,
for example, on pressing Enter).

## Technical requirements:

To complete the task you can use any framework of your choice with JavaScript or TypeScript.

The city suggestion should be implemented using [GeoDB API](http://geodb-cities-api.wirefreethought.com).
You don't need to register, by default a free plan is available for a public use.
Please consider that the API endpoint is limited to 1 request per second and a max of 86400 requests per day.
The user should not exhaust the limit while searching for city names. Check the [API documentation](http://geodb-cities-api.wirefreethought.com/docs/api/find-cities)
and [a response example](http://geodb-free-service.wirefreethought.com/v1/geo/cities?
namePrefix=helsinki&hateoasMode=false&limit=5&offset=0).

The widgets should be implemented using [Open weather map API](https://openweathermap.org).
Please use provided api key to query the API.
The API also has a limit of 60 calls per minute; please consider it in your implementation as well.
You need to [fetch current weather](https://openweathermap.org/current) for each given city.

Please use our [brand book](https://brandpad.io/ambine) to find graphic elements, fonts and colors codes.
Color codes:

- background: #fff6db
- search input background: #dbe1d5
- buttons: #0561ad
- icons: #5d2f8f

Icons:

- search: https://fontawesome.com/v5.15/icons/search?style=solid
- delete button: https://fontawesome.com/v5.15/icons/trash?style=solid
- plus symbol: https://fontawesome.com/v5.15/icons/plus?style=solid

You are free to use any development tools.
There are also no limitations on how the app is built and launched, whether it is create-react-app,
webpack, or just a bare HTML page. Please also provide instructions on how to launch the app.

You can also make any changes to the design if needed, for example,
using material UI components or different icons.

If you want to make an extra step, you may implement a mechanism to store widgets using, e.g., 
browser local storage and display existing widgets on page reload.
Consider API limitations in this case as well.


## Acceptance criteria:
- At least a part of the implementation should be completed.
- It should be possible to launch the app and open it in the browser.
- The app should work according to requirements.
- There should not be visible errors or debug in the console.
- Possible errors and exceptional situations should be handled (at least, the most critical ones)
- Unit tests.

## We definitely will pay attention to:
- Commits structure and messages
- Handling errors and exceptional situations
- Code style and readability
- Unit tests and test coverage
- A reasoning for types if TypeScript is used.

## What is not required:
- Responsiveness and mobile layout
- Pixel perfect layout
- Integration or acceptance tests
- Handling missing cities
- Handling duplicated cities
- Server-side, API


## Links and resources:
- [Design](https://xd.adobe.com/view/6fc37379-94f1-4d01-af28-8bdc2b8888e3-3a90/)
- [Ambine Brand book](https://brandpad.io/ambine)
- [Poppins font](https://fonts.google.com/specimen/Poppins)
- [Icons](https://fontawesome.com)
- [Weather icons](https://erikflowers.github.io/weather-icons/)
- [Open weather map API (current weather)](https://openweathermap.org/current)
- [GeoDB API](http://geodb-cities-api.wirefreethought.com)
- [How to search for cities using GeoDB API](http://geodb-cities-api.wirefreethought.com/docs/api/find-cities)